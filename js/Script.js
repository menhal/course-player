﻿var scriptclassid="0000000000-00002-00301-00001-0010100";
var scriptversion="1.1";
var matchpage=false;
var g_type="0000000000-00002-00301-00001-0010100";
var g_control=false;
var g_filepath="";
var g_listoutline=[];
var g_currentindex=-1;
var g_currentid=null;

var mp_State=false;
var web_State=false;
var record_sum_play=0;

$.get('Courseware.xml', function (data) {
    console.log(data)
})

function IsGoIn(obj)
{
    var mediaplayer = document.getElementById("MediaPlayer");
    mediaplayer.enabled=obj;
}

function OnDrag(a)
{
    var mediaplayer = document.getElementById("MediaPlayer");
    if(a==0)
    {
       mediaplayer.uiMode="none";
    }
    if(a==1)
    {
       mediaplayer.uiMode="full";
    }
     if(a==3)
    {
       return mediaplayer.uiMode;
    }
}

function OnPause(a)
{
    var mediaplayer = document.getElementById("MediaPlayer");
    if(a==0)
    {
       mediaplayer.controls.pause();
    }
    if(a==1)
    {
       mediaplayer.controls.play();
    }
}

function GetNowTime()
{
    var mediaplayer = document.getElementById("MediaPlayer");
    if(mp_State==true||mediaplayer.playState=='2'||mediaplayer.playState=='3') {//alert(mediaplayer.controls.currentPosition + '    '+mp_State);
       return mediaplayer.controls.currentPosition; }
    else 
       return -1; 
}

function SetNowTime(time)
{
    var mediaplayer = document.getElementById("MediaPlayer");
    mediaplayer.controls.currentPosition=time;
    mediaplayer.controls.play();
}

function GetIsSuccess()
{
    var mediaplayer = document.getElementById("MediaPlayer");
    if(mp_State==true) return 1; else return 0;
}

function GetSumTime()
{
   if(record_sum_play!=0) return record_sum_play;
   
    var mediaplayer = document.getElementById("MediaPlayer");
    return mediaplayer.currentMedia.duration;
}

window.onfocus = function (e) { OnPause(1);};
window.onactivate = function (e) { OnPause(1);};

function ListAdd(items,obj)
{
    items[items.length]=obj;
}

function ControlCenter()
{
    this.PAPI=null;
    this.PAPIControl=false;
    this.PAPIVersion="";
    this.PAPITriesFind=0;
    this.PAPITriesFindMax=500;
    this.PDocumentSizeWidth=640;
    this.PDocumentSizeHeight=480;
}

function ControlCurrent()
{
    this.PIndexOutline=-1;
    this.PIndexDocumentBoard=-1;
    this.PIndexDocumentContent=-1;
    this.PIndexDocumentPowerPoint=-1;
    this.PIndexContainer=-1;
}

function CoursewareHead()
{
    this.PType="";
    this.PName="";
    this.PAuthor="";
    this.PEditor="";
    this.PKeywords="";
    this.PDuration="";
    this.PCopyright="";
    this.PDescription="";
}

function MediaObject()
{
    this.PUrl="";
    this.PEnabled=false;
    this.PWidth=0;
    this.PHeight=0;
    this.PISIN=true;
}

function CoursewareMedia()
{
    this.PAudio=new MediaObject();
    this.PVideo=new MediaObject();
    this.PMovie=new MediaObject();
}

function OutlineItem()
{
    this.PText="";
    this.PValue="";
    this.PLevel=1;
    this.PPosition=0;
}

function TrackOutline()
{
    this.PItems=[];
}

function DocumentBoardItem()
{
    this.PType="";
    this.PImage=false;
    this.PCaption="";
    this.PLinkUrl="";
    this.PPosition=0;
    this.PDuration=0;
    this.PScrollTop=0;
    this.PScrollLeft=0;
}

function DocumentBoard()
{
    this.PItems=[];
}

function DocumentContentItem()
{
    this.PType="";
    this.PImage=false;
    this.PCaption="";
    this.PLinkUrl="";
    this.PPosition=0;
    this.PDuration=0;
    this.PScrollTop=0;
    this.PScrollLeft=0;
}

function DocumentContent()
{
    this.PItems=[];
}

function DocumentPowerPointItem()
{
    this.PType="";
    this.PImage=false;
    this.PCaption="";
    this.PLinkUrl="";
    this.PPosition=0;
    this.PDuration=0;
    this.PScrollTop=0;
    this.PScrollLeft=0;
}

function DocumentPowerPoint()
{
    this.PItems=[];
}

function TrackDocument()
{
    this.PBoard=new DocumentBoard();
    this.PContent=new DocumentContent();
    this.PPowerPoint=new DocumentPowerPoint();
}

function ContainerItem()
{
    this.PFrame="";
    this.PPosition=0;
    this.PDuration=0;
    this.PScrollTop=0;
    this.PScrollLeft=0;
}

function TrackContainer()
{
    this.PItems=[];
}

function CoursewareTrack()
{
    this.POutline=new TrackOutline();
    this.PDocument=new TrackDocument();
    this.PContainer=new TrackContainer();
}

function ControlCourseware()
{
    this.PMode="";
    this.PHead=new CoursewareHead();
    this.PMedia=new CoursewareMedia();
    this.PTrack=new CoursewareTrack();
}

function AppControl()
{
    this.PCenter=new ControlCenter();
    this.PCurrent=new ControlCurrent();
    this.PCourseware=new ControlCourseware();
}

var pappcontrol=new AppControl();

function outlinelist_click(index)
{
    var media=document.getElementById("MediaPlayer");
    if(media.enabled==false) return false;//添加判断，当播放器为不可拖动的情况下，点目录不可变化
    var position=pappcontrol.PCourseware.PTrack.POutline.PItems[index].PPosition;
    document.getElementById("MediaPlayer").controls.currentPosition=(position+2)/1000.0;
    outlinelist_select(index);
}

function outlinelist_select(index)
{
    outlinelist_unselect();
    pappcontrol.PCurrent.PIndexOutline=index;
    var outlinelistitem=document.getElementById("outlinelistitem"+index);
    var outlineitem=pappcontrol.PCourseware.PTrack.POutline.PItems[index];
    if(pappcontrol.PCenter.PAPIControl) parent.SetOutline(outlineitem.PValue);
    document.getElementById("CurrentCaption").innerText=outlineitem.PValue;
    outlinelistitem.className="ItemActive";
}

function outlinelist_unselect()
{
    var nodes=document.getElementById("CoursewareList").childNodes;
    for(var i=0;i<nodes.length;i++)
    {
        if(nodes[i].className=="ItemActive") nodes[i].className="ItemNormal";
    }
    pappcontrol.PCurrent.PIndexOutline=-1;
}

function documentboardlist_select(index)
{
    pappcontrol.PCurrent.PIndexDocumentBoard=index;
    var documentitem=pappcontrol.PCourseware.PTrack.PDocument.PBoard.PItems[index];
    var width=pappcontrol.PCenter.PDocumentSizeWidth;
    var height=pappcontrol.PCenter.PDocumentSizeHeight;
    var documenthtml="&nbsp;";
    if(documentitem.PImage)
    {
        documenthtml="<img src='"+documentitem.PLinkUrl+"' style='width: "+width+"px;height: "+height+"px;' alt='"+documentitem.PPosition+"' />";
    }
    else
    {
        documenthtml="<iframe src='"+documentitem.PLinkUrl+"' style='width: "+width+"px;height: "+height+"px;' frameBorder='0' scrolling='auto'></iframe>";
    }
    
    document.getElementById("DocumentBoard").innerHTML=documenthtml;
}

function documentcontentlist_select(index)
{
    pappcontrol.PCurrent.PIndexDocumentContent=index;
    var documentitem=pappcontrol.PCourseware.PTrack.PDocument.PContent.PItems[index];
    var width=pappcontrol.PCenter.PDocumentSizeWidth;
    var height=pappcontrol.PCenter.PDocumentSizeHeight;
    var documenthtml="&nbsp;";
    if(documentitem.PImage)
    {
        documenthtml="<img src='"+documentitem.PLinkUrl+"' style='width: "+width+"px;height: "+height+"px;' alt='"+documentitem.PPosition+"' />";
    }
    else
    {
        documenthtml="<iframe src='"+documentitem.PLinkUrl+"' style='width: "+width+"px;height: "+height+"px;' frameBorder='0' scrolling='auto'></iframe>";
    }
    
    document.getElementById("DocumentContent").innerHTML=documenthtml;
}

function documentpowerpointlist_select(index)
{
    pappcontrol.PCurrent.PIndexDocumentPowerPoint=index;
    var documentitem=pappcontrol.PCourseware.PTrack.PDocument.PPowerPoint.PItems[index];
    var width=pappcontrol.PCenter.PDocumentSizeWidth;
    var height=pappcontrol.PCenter.PDocumentSizeHeight;
    var documenthtml="&nbsp;";
    if(documentitem.PImage)
    {
        documenthtml="<img src='"+documentitem.PLinkUrl+"' style='width: "+width+"px;height: "+height+"px;' alt='"+documentitem.PPosition+"' />";
        
    }
    else
    {
        documenthtml="<iframe src='"+documentitem.PLinkUrl+"' style='width: "+width+"px;height: "+height+"px;' frameBorder='0' scrolling='auto'></iframe>";
        
    }
    
    document.getElementById("DocumentPowerPoint").innerHTML=documenthtml;
}

function containerlist_select(index)
{
    pappcontrol.PCurrent.PIndexContainer=index;
    var containeritem=pappcontrol.PCourseware.PTrack.PContainer.PItems[index];
    switch(containeritem.PFrame)
    {
        case "PowerPoint":
            document.getElementById("DocumentContent").style.display="none";
            document.getElementById("DocumentBoard").style.display="none";
            document.getElementById("DocumentPowerPoint").style.display="block";
            break;
        case "Content":
            document.getElementById("DocumentPowerPoint").style.display="none";
            document.getElementById("DocumentBoard").style.display="none";
            document.getElementById("DocumentContent").style.display="block";
            break;
        case "Board":
            document.getElementById("DocumentPowerPoint").style.display="none";
            document.getElementById("DocumentContent").style.display="none";
            document.getElementById("DocumentBoard").style.display="block";
            break;
        default:
            document.getElementById("DocumentContent").style.display="none";
            document.getElementById("DocumentBoard").style.display="none";
            document.getElementById("DocumentPowerPoint").style.display="block";
            break;
    }
}

function GetCurrentPosition(mediaplayer)
{
    var currentposition=0;
    try
    {
        currentposition=mediaplayer.controls.currentPosition*1000;
    }
    catch(e)
    {
        currentposition=-1;
    }
    return currentposition;
}

function Return_string(str)
{
    var a=str.split(":");
    var result;
    if(a.length==2)
    {
        result=parseInt(a[1])+1;
        if(result<9)
        {
           result="0"+result;
        }
        result=a[0]+":"+result;
    }
    else if(a.length==3)
    {
        result=parseInt(a[2])+1;
        if(result<9)
        {
           result="0"+result;
        }
        result=a[0]+":"+a[1]+":"+result;
    }
    return result;
}

function timeout_tick()
{
    var cameraplayer=document.getElementById("MediaPlayer");
    var currentposition=GetCurrentPosition(cameraplayer);
    if(currentposition==-1) return;
    var count=0;
    var index=-2;
    var currentindex=-1;
    var outlineitems=pappcontrol.PCourseware.PTrack.POutline.PItems;
    var documentboarditems=pappcontrol.PCourseware.PTrack.PDocument.PBoard.PItems;
    var documentcontentitems=pappcontrol.PCourseware.PTrack.PDocument.PContent.PItems;
    var documentpowerpointitems=pappcontrol.PCourseware.PTrack.PDocument.PPowerPoint.PItems;
    var containeritems=pappcontrol.PCourseware.PTrack.PContainer.PItems;

    var media=document.getElementById("MediaPlayer");  
    var div=document.getElementById("div1");
    if(div.innerText=="总时间：00:00")
    {
        div.innerText="总时间："+media.currentMedia.durationString;
    }
    
    if(record_sum_play ==0){
       record_sum_play=GetSumTime();}
       
    count=outlineitems.length;
    index=-2;
    currentindex=pappcontrol.PCenter.PCurrentOutlineIndex;
    for(var i=0;i<count;i++)
    {
        if(i==count-1 && outlineitems[i].PPosition<=currentposition)
        {
            if(currentindex!=i) index=i;
        }
        else if(outlineitems[i].PPosition<=currentposition && currentposition<outlineitems[i+1].PPosition)
        {
            if(currentindex!=i) index=i;
        }
    }
    if(currentindex!=index && index!=-2)
    {
        outlinelist_select(index);
    }
    
    count=documentboarditems.length;
    index=-2;
    currentindex=pappcontrol.PCurrent.PIndexDocumentBoard;
    for(var i=0;i<count;i++)
    {
        if(i==count-1 && documentboarditems[i].PPosition<=currentposition)
        {
            if(currentindex!=i) index=i;
        }
        else if(documentboarditems[i].PPosition<=currentposition && currentposition<documentboarditems[i+1].PPosition)
        {
            if(currentindex!=i) index=i;
        }
    }
    if(currentindex!=index && index!=-2)
    {
        documentboardlist_select(index);
    }
    
    count=documentcontentitems.length;
    index=-2;
    currentindex=pappcontrol.PCurrent.PIndexDocumentContent;
    for(var i=0;i<count;i++)
    {
        if(i==count-1 && documentcontentitems[i].PPosition<=currentposition)
        {
            if(currentindex!=i) index=i;
        }
        else if(documentcontentitems[i].PPosition<=currentposition && currentposition<documentcontentitems[i+1].PPosition)
        {
            if(currentindex!=i) index=i;
        }
    }
    if(currentindex!=index && index!=-2)
    {
        documentcontentlist_select(index);
    }
    
    count=documentpowerpointitems.length;
    index=-2;
    currentindex=pappcontrol.PCurrent.PIndexDocumentPowerPoint;
    for(var i=0;i<count;i++)
    {
        if(i==count-1 && documentpowerpointitems[i].PPosition<=currentposition)
        {
            if(currentindex!=i) index=i;
        }
        else if(documentpowerpointitems[i].PPosition<=currentposition && currentposition<documentpowerpointitems[i+1].PPosition)
        {
            if(currentindex!=i) index=i;
        }
    }
    if(currentindex!=index && index!=-2)
    {
        documentpowerpointlist_select(index);
    }
    
    count=containeritems.length;
    index=-2;
    currentindex=pappcontrol.PCurrent.PIndexContainer;
    for(var i=0;i<count;i++)
    {
        if(i==count-1 && containeritems[i].PPosition<=currentposition)
        {
            if(currentindex!=i) index=i;
        }
        else if(containeritems[i].PPosition<=currentposition && currentposition<containeritems[i+1].PPosition)
        {
            if(currentindex!=i) index=i;
        }
    }
    if(currentindex!=index && index!=-2)
    {
        containerlist_select(index);
    }
}

function timeout_track()
{
    var cameraplayer=document.getElementById("MediaPlayer");
    var currentposition=GetCurrentPosition(cameraplayer);
    if(currentposition==-1) return;
    var count=0;
    var index=-2;
    var currentindex=-1;
    var outlineitems=pappcontrol.PCourseware.PTrack.POutline.PItems;
    var documentboarditems=pappcontrol.PCourseware.PTrack.PDocument.PBoard.PItems;
    var documentcontentitems=pappcontrol.PCourseware.PTrack.PDocument.PContent.PItems;
    var documentpowerpointitems=pappcontrol.PCourseware.PTrack.PDocument.PPowerPoint.PItems;
    var containeritems=pappcontrol.PCourseware.PTrack.PContainer.PItems;
    var mediaplayer=cameraplayer;
              
     //显示总时间与当前时间
//    var media=document.getElementById("MediaPlayer");
//    var div=document.getElementById("div1");
//    var value_now= media.currentMedia.durationString;
//    if(value_now=="00:00")
//    {
//       div.innerText="00:00"+"/"+media.currentMedia.durationString;
//    }
//    else
//    {
//       div.innerText=Return_string(media.controls.currentPositionString)+"/"+media.currentMedia.durationString;
//    }
    
    var media=document.getElementById("MediaPlayer");  
    var div=document.getElementById("div1");
    if(div.innerText=="总时间：00:00")
    {
        div.innerText="总时间："+media.currentMedia.durationString;
    }
    
    parent.TrackPosition(mediaplayer.controls.currentPosition);
    if(mediaplayer.playState==3) parent.TrackLengthPlay(1);

    count=outlineitems.length;
    index=-2;
    currentindex=pappcontrol.PCenter.PCurrentOutlineIndex;
    for(var i=0;i<count;i++)
    {
        if(i==count-1 && outlineitems[i].PPosition<=currentposition)
        {
            if(currentindex!=i) index=i;
        }
        else if(outlineitems[i].PPosition<=currentposition && currentposition<outlineitems[i+1].PPosition)
        {
            if(currentindex!=i) index=i;
        }
    }
    if(currentindex!=index && index!=-2)
    {
        outlinelist_select(index);
    }

    count=documentboarditems.length;
    index=-2;
    currentindex=pappcontrol.PCurrent.PIndexDocumentBoard;
    for(var i=0;i<count;i++)
    {
        if(i==count-1 && documentboarditems[i].PPosition<=currentposition)
        {
            if(currentindex!=i) index=i;
        }
        else if(documentboarditems[i].PPosition<=currentposition && currentposition<documentboarditems[i+1].PPosition)
        {
            if(currentindex!=i) index=i;
        }
    }
    if(currentindex!=index && index!=-2)
    {
        documentboardlist_select(index);
    }
    
    count=documentcontentitems.length;
    index=-2;
    currentindex=pappcontrol.PCurrent.PIndexDocumentContent;
    for(var i=0;i<count;i++)
    {
        if(i==count-1 && documentcontentitems[i].PPosition<=currentposition)
        {
            if(currentindex!=i) index=i;
        }
        else if(documentcontentitems[i].PPosition<=currentposition && currentposition<documentcontentitems[i+1].PPosition)
        {
            if(currentindex!=i) index=i;
        }
    }
    if(currentindex!=index && index!=-2)
    {
        documentcontentlist_select(index);
    }
    
    count=documentpowerpointitems.length;
    index=-2;
    currentindex=pappcontrol.PCurrent.PIndexDocumentPowerPoint;
    for(var i=0;i<count;i++)
    {
        if(i==count-1 && documentpowerpointitems[i].PPosition<=currentposition)
        {
            if(currentindex!=i) index=i;
        }
        else if(documentpowerpointitems[i].PPosition<=currentposition && currentposition<documentpowerpointitems[i+1].PPosition)
        {
            if(currentindex!=i) index=i;
        }
    }
    if(currentindex!=index && index!=-2)
    {
        documentpowerpointlist_select(index);
    }
    
    count=containeritems.length;
    index=-2;
    currentindex=pappcontrol.PCurrent.PIndexContainer;
    for(var i=0;i<count;i++)
    {
        if(i==count-1 && containeritems[i].PPosition<=currentposition)
        {
            if(currentindex!=i) index=i;
        }
        else if(containeritems[i].PPosition<=currentposition && currentposition<containeritems[i+1].PPosition)
        {
            if(currentindex!=i) index=i;
        }
    }
    if(currentindex!=index && index!=-2)
    {
        containerlist_select(index);
    }
}

function OnCameraPlayStateChage(NewState)
{
//    if(9==NewState||10==NewState)
//    {
//        if(g_control) parent.SetOutline("");
//        listoutline_unselect();
//        document.getElementById("CoursewareMatter").innerHTML="&nbsp;";
//    }
}

function OnCameraMediaChange()
{
    try
    {
        //document.getElementById("CoursewareDuration").innerHTML=document.getElementById("CameraPlayer").currentMedia.durationString;
    }
    catch(e){}
}

function autoresize()
{
}

function TabShift(i)
{
    var v=document.getElementById("TabV");
    var a=document.getElementById("TabA");
	
    var player = document.getElementById("MediaPlayer");
    var img=document.getElementById("IsIN");
	
    document.getElementById("AuthorLogo").style.display='none';
    document.getElementById("MediaPlayer").style.display='block';
            
	if(pappcontrol.PCourseware.PMedia.PAudio.PISIN=="false")
	{
	    if(i == 1)
	    {
		   v.src="img/TabBG_A_1.gif";
		   a.src="img/TabBG_A_2.gif";
		   //player.setAttribute("url","Media/Video.asx");
		   img.style.display="block";
	     }
	     if(i == 2)
	     {
		   v.src="img/TabBG_B_1.gif";
		   a.src="img/TabBG_B_2.gif";
		   //player.setAttribute("url","Media/Audio.asx");
           img.style.display="none";
           
          if(OnDrag(3)=='none')  
          {
            document.getElementById("AuthorLogo").style.display='block';
            document.getElementById("MediaPlayer").style.display='none';
          }                                                 
	     }
	}
	else
	{
	     if(i == 1)
	     {
		   v.src="img/TabBG_A_1.gif";
		   a.src="img/TabBG_A_2.gif";
		   //player.setAttribute("url","Media/Video.asx");
	       AdjustMediaPlayer(300);
	     }
	     if(i == 2)
	     {
		   v.src="img/TabBG_B_1.gif";
		   a.src="img/TabBG_B_2.gif";
		   //player.setAttribute("url","Media/Audio.asx");
		   AdjustMediaPlayer(50);
		   			   
          if(OnDrag(3)=='none')  
          {
            document.getElementById("AuthorLogo").style.display='block';
            document.getElementById("MediaPlayer").style.display='none';
          }                                                 
	     }
	}
}

function Shift(o,t)
{
    var i=document.getElementById(t);
    var v=document.getElementById(o);
    var l=document.getElementById("outline");
    var c=document.getElementById("CoursewareContent");
    if(i!=null && i!=undefined)
    {		
        if(v.style.display=="none")
        {
            i.src="img/minIco.gif";
            i.alt="关闭";
            if(v!=null && v!=undefined) v.style.display="";	
            if(v.id=="video")
            {
                l.style.height="296px";
                c.style.height="232px";
            }
        }
        else
        {
            i.src="img/maxIco.gif";
            i.alt="展开";
            if(v!=null && v!=undefined)
            v.style.display="none";	
            if(v.id=="video")
            {
                l.style.height="534px";
                c.style.height="470px";
            }
        }
    }
}

function AdjustMediaPlayer(height)
{
	var mediaplayer = document.getElementById("MediaPlayer"); 
	var logo=document.getElementById("AuthorLogo");
	mediaplayer.style["height"]=height;
	logo.style["height"]=300-height;
}

function ScanForAPI(win)
{
    while((win.API_AC_2012_11==null)&&(win.parent!=null)&&(win.parent!=win))
    {
        pappcontrol.PCenter.PAPITriesFind++;
        if(pappcontrol.PCenter.PAPITriesFind>pappcontrol.PCenter.PAPITriesFindMax)
        {
             return null;
        }
        win=win.parent;
    }
    return win.API_AC_2012_11;
}

function GetAPI(win)
{
    var API=null;
    if((win.parent!=null)&&(win.parent!=win))
    {
        API=ScanForAPI(win.parent);
    }
    if((API==null)&&(win.opener!=null))
    {
        API=ScanForAPI(win.opener);
    }
    if(API!=null)
    {
        pappcontrol.PCenter.PAPI=API;
        pappcontrol.PCenter.PAPIControl=true;
        pappcontrol.PCenter.PAPIVersion=API.PVersion;
    }
}

function initcontrol()
{
    
    var control=parent.pcontrol;
    if(control==null || control==undefined)
    {
        autoresize();
        setInterval(timeout_tick,1000);
    }
    else
    {
        pappcontrol.PCenter.PAPIControl=true;
        
        autoresize();
        
//        document.getElementById("CoursewareHeader").style.display="none";
//        document.getElementById("CoursewareFooter").style.display="none";
        parent.SetMediaPlayer(document.getElementById("MediaPlayer"));
        var record=parent.pcontrol.PRecord;
        if(record)
        {
            parent.pcontrol.PType=g_type;
            parent.pcontrol.PRecordType="PLAY";
            setInterval(timeout_track,1000);
        }
        else
        {
            setInterval(timeout_tick,1000);
        }
        parent.PromptPosition();
    }
}

function LoadXml(xmlfile)
{
    var xd=null;
    try //Internet Explorer
    {
        xd=new ActiveXObject("Microsoft.XMLDOM");
        xd.async=false;
        xd.load(xmlfile);
    }
    catch(e)
    {
        try //Firefox, Mozilla, Opera, etc.
        {
            xd=document.implementation.createDocument("","",null);
            xd.async=false;
            xd.load(xmlfile);
        }
        catch(e)   
        {
            try //Google Chrome
            {
                var xmlhttp = new window.XMLHttpRequest();
                xmlhttp.open("GET",xmlfile,false);
                xmlhttp.send(null);
                xd = xmlhttp.responseXML;
                //xd = xmlhttp.responseXML.documentElement;
            }   
            catch(e)   
            {
            }
        }
    }
    return xd;
}

function initcontent()
{
    var xmlfile="Courseware.xml";
    var index=0;
    var xd=null;
    
    index=xmlfile.lastIndexOf('/');
    if(index>=0) g_filepath=xmlfile.substring(0, index+1);
    else g_filepath="";
    
    xd=LoadXml(xmlfile);

    if(xd==null)
    {
        //alert("无法解析Xml文件或文件不存在！");
        return;
    }
    if(xd.documentElement==null)
    {
        //alert("无法解析Xml文件或文件不存在！");
        return;
    }
    
    var root=xd.documentElement;
    var coursewarename=document.getElementById("CoursewareName");
    var coursewareauthor=document.getElementById("CoursewareAuthor");
    var outlinelist=document.getElementById("CoursewareList");
    var path="";
    var node=null;
    var nodes=null;
    var count=0;
    
    
    var j=0;
//    var obj=null;

    var outlineindex=0;
    var outlinelevel=[];
    var outlinelevelnumber="";
    var outlinelevelspace="&nbsp;&nbsp;";
    var outlinelevelspacestring="";
    
    var xnoutlinetext=null;
    var xnoutlinelevel=null;
    var xnoutlineposition=null;
    
    var pageoutlinetext="";
    var pageoutlinelevel=1;
    var pageoutlineposition=0;
    
    var xndocumentboardtype=null;
    var xndocumentboardcaption=null;
    var xndocumentboardlinkurl=null;
    var xndocumentboardposition=null;
    var xndocumentboardduration=null;
    var xndocumentboardscrolltop=null;
    var xndocumentboardscrollleft=null;
    
    var pagedocumentboardtype="";
    var pagedocumentboardcaption="";
    var pagedocumentboardlinkurl="";
    var pagedocumentboardposition=0;
    var pagedocumentboardduration=0;
    var pagedocumentboardscrolltop=0;
    var pagedocumentboardscrollleft=0;
    
    var xndocumentcontenttype=null;
    var xndocumentcontentcaption=null;
    var xndocumentcontentlinkurl=null;
    var xndocumentcontentposition=null;
    var xndocumentcontentduration=null;
    var xndocumentcontentscrolltop=null;
    var xndocumentcontentscrollleft=null;
    
    var pagedocumentcontenttype="";
    var pagedocumentcontentcaption="";
    var pagedocumentcontentlinkurl="";
    var pagedocumentcontentposition=0;
    var pagedocumentcontentduration=0;
    var pagedocumentcontentscrolltop=0;
    var pagedocumentcontentscrollleft=0;
    
    var xndocumentpowerpointtype=null;
    var xndocumentpowerpointcaption=null;
    var xndocumentpowerpointlinkurl=null;
    var xndocumentpowerpointposition=null;
    var xndocumentpowerpointduration=null;
    var xndocumentpowerpointscrolltop=null;
    var xndocumentpowerpointscrollleft=null;
    
    var pagedocumentpowerpointtype="";
    var pagedocumentpowerpointcaption="";
    var pagedocumentpowerpointlinkurl="";
    var pagedocumentpowerpointposition=0;
    var pagedocumentpowerpointduration=0;
    var pagedocumentpowerpointscrolltop=0;
    var pagedocumentpowerpointscrollleft=0;
    
    var xncontainerframe=null;
    var xncontainerposition=null;
    var xncontainerduration=null;
    var xncontainerscrolltop=null;
    var xncontainerscrollleft=null;
    
    var pagecontainerframe="";
    var pagecontainerposition=0;
    var pagecontainerduration=0;
    var pagecontainerscrolltop=0;
    var pagecontainerscrollleft=0;
    
    path="title";
    node=root.selectSingleNode(path);
    pappcontrol.PCourseware.PHead.PName=node.text;
    coursewarename.innerHTML=node.text;
    
    path="author";
    node=root.selectSingleNode(path);
    pappcontrol.PCourseware.PHead.PEditor=node.text;
    
    path="keywords";
    node=root.selectSingleNode(path);
    pappcontrol.PCourseware.PHead.PKeywords=node.text;
    
    path="copyright";
    node=root.selectSingleNode(path);
    pappcontrol.PCourseware.PHead.PCopyright=node.text;
    
    //var copyright=document.createElement("div");
    //copyright.className="Copyright";
    //copyright.innerHTML="版权："+node.text;
    //copyright.innerHTML="Copyright: "+node.text;
    //outlinelist.appendChild(copyright);
    var copyright=document.getElementById("Copyright");
    //copyright.innerHTML="版权："+node.text;
    
//    path="media/audio";
//    node=root.selectSingleNode(path);
//    pappcontrol.PCourseware.PMedia.PAudio.PUrl="Movie.wmv";
//    pappcontrol.PCourseware.PMedia.PAudio.PEnabled=node.getAttribute("enabled")=="true";
//    pappcontrol.PCourseware.PMedia.PAudio.PWidth=parseInt(node.getAttribute("width"));
//    pappcontrol.PCourseware.PMedia.PAudio.PHeight=parseInt(node.getAttribute("height"));
//    pappcontrol.PCourseware.PMedia.PAudio.PISIN=node.getAttribute("IsIn");
    
//    path="media/video";
//    node=root.selectSingleNode(path);
//    pappcontrol.PCourseware.PMedia.PVideo.PUrl=node.text;
//    pappcontrol.PCourseware.PMedia.PVideo.PEnabled=node.getAttribute("enabled")=="true";
//    pappcontrol.PCourseware.PMedia.PVideo.PWidth=parseInt(node.getAttribute("width"));
//    pappcontrol.PCourseware.PMedia.PVideo.PHeight=parseInt(node.getAttribute("height"));
    
//    path="media/movie";
//    node=root.selectSingleNode(path);
    pappcontrol.PCourseware.PMedia.PMovie.PUrl="Movie.wmv";
    pappcontrol.PCourseware.PMedia.PMovie.PEnabled=node.getAttribute("enabled")=="true";
    pappcontrol.PCourseware.PMedia.PMovie.PWidth=parseInt(node.getAttribute("width"));
    pappcontrol.PCourseware.PMedia.PMovie.PHeight=parseInt(node.getAttribute("height"));
        
    path="content";
    nodes=root.selectNodes(path);
    count=nodes.length;
    index=1;
    
    for(var i=0;i<count;i++)
    {
        node=nodes[i];
        
        xnoutlinetext=node.selectSingleNode("title");
//        xnoutlinelevel=node.selectSingleNode("level");
        xnoutlineposition=node.selectSingleNode("timePos");

        if(xnoutlinetext.text=="")
        {
            continue;
        }
                
        pageoutlinetext=xnoutlinetext.text;
        pageoutlinevalue=xnoutlinetext.text;
        pageoutlinelevel=parseInt(1);
        pageoutlineposition=parseInt(xnoutlineposition.text);
        
        outlinelevelnumber="";
        outlinelevelspacestring="";
        if(pageoutlinelevel<1) pageoutlinelevel=1;
        for(j=1;j<pageoutlinelevel;j++)
        {
            outlinelevelspacestring=outlinelevelspacestring+outlinelevelspace;
        }
        if(pageoutlinelevel<2)
        {
            outlinelevel[pageoutlinelevel-1]=index;
            outlinelevelnumber=index;
//            pageoutlinetext=outlinelevelnumber+". "+xnoutlinetext.text;
//            pageoutlinevalue=outlinelevelnumber+". "+xnoutlinetext.text;
            pageoutlinetext=xnoutlinetext.text;
            pageoutlinevalue=xnoutlinetext.text;
            for(j=1;j<outlinelevel.length;j++)
            {
                outlinelevel[j]=0;
            }
            //pageoutlinetext = xnoutlinetext.text;
        }
        else
        {
            if(outlinelevel.length<pageoutlinelevel) outlinelevel[pageoutlinelevel-1]=0;
            outlinelevel[pageoutlinelevel-1]=outlinelevel[pageoutlinelevel-1]+1;
            for(j=0;j<pageoutlinelevel-1;j++)
            {
                outlinelevelnumber=outlinelevelnumber+outlinelevel[j]+".";
            }
            outlinelevelnumber=outlinelevelnumber+outlinelevel[pageoutlinelevel-1];
//            pageoutlinetext=outlinelevelspacestring+outlinelevelnumber+xnoutlinetext.text;
//            pageoutlinevalue=outlinelevelnumber+xnoutlinetext.text;
            pageoutlinetext=outlinelevelspacestring+xnoutlinetext.text;
            pageoutlinevalue=xnoutlinetext.text;
        }
        
        var obj=new OutlineItem();
        obj.PText=pageoutlinetext;
        obj.PValue=pageoutlinevalue;
        obj.PLevel=pageoutlinelevel;
        obj.PPosition=pageoutlineposition;
        
//        if(obj!=null)
//        {
            ListAdd(pappcontrol.PCourseware.PTrack.POutline.PItems,obj);
            var item=document.createElement("div");
            item.className="ItemNormal";
            item.id="outlinelistitem"+outlineindex;
            item.innerHTML=pageoutlinetext;
            item.onclick=new Function("javascript:outlinelist_click("+outlineindex+");");
            outlinelist.appendChild(item);
            outlineindex++;
            if(pageoutlinelevel<2) index++;
//        }
    }
    
//    path = "track/document/board/item";
//    nodes=root.selectNodes(path);
//    count=nodes.length;
//    index=1;
//    
//    for(var i=0;i<count;i++)
//    {
//        node = nodes[i];
//        
//        xndocumentboardtype=node.selectSingleNode("type");
//        xndocumentboardcaption=node.selectSingleNode("caption");
//        xndocumentboardlinkurl=node.selectSingleNode("linkurl");
//        xndocumentboardposition=node.selectSingleNode("position");
//        xndocumentboardduration=node.selectSingleNode("duration");
//        xndocumentboardscrolltop=node.selectSingleNode("scrolltop");
//        xndocumentboardscrollleft=node.selectSingleNode("scrollleft");
//        
//        pagedocumentboardtype=xndocumentboardtype.text;
//        pagedocumentboardcaption=xndocumentboardcaption.text;
//        pagedocumentboardlinkurl=xndocumentboardlinkurl.text;
//        pagedocumentboardposition=parseInt(xndocumentboardposition.text);
//        pagedocumentboardduration=parseInt(xndocumentboardduration.text);
//        pagedocumentboardscrolltop=parseInt(xndocumentboardscrolltop.text);
//        pagedocumentboardscrollleft=parseInt(xndocumentboardscrollleft.text);
//        
//        var obj=new DocumentBoardItem();
//        obj.PType=pagedocumentboardtype;
//        obj.PImage=pagedocumentboardtype=="Image";
//        obj.PCaption=pagedocumentboardcaption;
//        obj.PLinkUrl=pagedocumentboardlinkurl;
//        obj.PPosition=pagedocumentboardposition;
//        obj.PDuration=pagedocumentboardduration;
//        obj.PScrollTop=pagedocumentboardscrolltop;
//        obj.PScrollLeft=pagedocumentboardscrollleft;
//        
////        if(obj!=null)
////        {
//            ListAdd(pappcontrol.PCourseware.PTrack.PDocument.PBoard.PItems,obj);
////        }
//    }
//    
//    path = "track/document/content/item";
//    nodes=root.selectNodes(path);
//    count=nodes.length;
//    index=1;
//    
//    for(var i=0;i<count;i++)
//    {
//        node = nodes[i];
//        
//        xndocumentcontenttype=node.selectSingleNode("type");
//        xndocumentcontentcaption=node.selectSingleNode("caption");
//        xndocumentcontentlinkurl=node.selectSingleNode("linkurl");
//        xndocumentcontentposition=node.selectSingleNode("position");
//        xndocumentcontentduration=node.selectSingleNode("duration");
//        xndocumentcontentscrolltop=node.selectSingleNode("scrolltop");
//        xndocumentcontentscrollleft=node.selectSingleNode("scrollleft");
//        
//        pagedocumentcontenttype=xndocumentcontenttype.text;
//        pagedocumentcontentcaption=xndocumentcontentcaption.text;
//        pagedocumentcontentlinkurl=xndocumentcontentlinkurl.text;
//        pagedocumentcontentposition=parseInt(xndocumentcontentposition.text);
//        pagedocumentcontentduration=parseInt(xndocumentcontentduration.text);
//        pagedocumentcontentscrolltop=parseInt(xndocumentcontentscrolltop.text);
//        pagedocumentcontentscrollleft=parseInt(xndocumentcontentscrollleft.text);
//        
//        var obj=new DocumentContentItem();
//        obj.PType=pagedocumentcontenttype;
//        obj.PImage=pagedocumentcontenttype=="Image";
//        obj.PCaption=pagedocumentcontentcaption;
//        obj.PLinkUrl=pagedocumentcontentlinkurl;
//        obj.PPosition=pagedocumentcontentposition;
//        obj.PDuration=pagedocumentcontentduration;
//        obj.PScrollTop=pagedocumentcontentscrolltop;
//        obj.PScrollLeft=pagedocumentcontentscrollleft;
//        
////        if(obj!=null)
////        {
//            ListAdd(pappcontrol.PCourseware.PTrack.PDocument.PContent.PItems,obj);
////        }
//    }
    
    path = "sami";
    nodes=root.selectNodes(path);
    count=nodes.length;
    index=1;
    
    for(var i=0;i<count;i++)
    {
        node = nodes[i];
        
        xndocumentpowerpointtype=node.selectSingleNode("type");
//        xndocumentpowerpointcaption=node.selectSingleNode("caption");
        xndocumentpowerpointlinkurl=node.selectSingleNode("url");
        xndocumentpowerpointposition=node.selectSingleNode("start");
        xndocumentpowerpointduration=node.selectSingleNode("contentTimePos");
//        xndocumentpowerpointscrolltop=node.selectSingleNode("scrolltop");
//        xndocumentpowerpointscrollleft=node.selectSingleNode("scrollleft");
        
        pagedocumentpowerpointtype=xndocumentpowerpointtype.text;
//        pagedocumentpowerpointcaption=xndocumentpowerpointcaption.text;
        pagedocumentpowerpointlinkurl=xndocumentpowerpointlinkurl.text;
        pagedocumentpowerpointposition=parseInt(xndocumentpowerpointposition.text);
        pagedocumentpowerpointduration=parseInt(xndocumentpowerpointduration.text);
        pagedocumentpowerpointscrolltop=parseInt(0);
        pagedocumentpowerpointscrollleft=parseInt(0);
        
        var obj=new DocumentPowerPointItem();
        obj.PType=pagedocumentpowerpointtype;
        obj.PImage=pagedocumentpowerpointtype=="1";
//        obj.PCaption=pagedocumentpowerpointcaption;
        obj.PLinkUrl=pagedocumentpowerpointlinkurl;
        obj.PPosition=pagedocumentpowerpointposition;
        obj.PDuration=pagedocumentpowerpointduration;
        obj.PScrollTop=pagedocumentpowerpointscrolltop;
        obj.PScrollLeft=pagedocumentpowerpointscrollleft;
        
//        if(obj!=null)
//        {
            ListAdd(pappcontrol.PCourseware.PTrack.PDocument.PPowerPoint.PItems,obj);
//        }
    }
    
//    path = "track/container/item";
//    nodes=root.selectNodes(path);
//    count=nodes.length;
//    index=1;
//    
//    for(var i=0;i<count;i++)
//    {
//        node = nodes[i];
//        
//        xncontainerframe=node.selectSingleNode("frame");
//        xncontainerposition=node.selectSingleNode("position");
//        xncontainerduration=node.selectSingleNode("duration");
//        xncontainerscrolltop=node.selectSingleNode("scrolltop");
//        xncontainerscrollleft=node.selectSingleNode("scrollleft");
        
        pagecontainerframe="PowerPoint";
        pagecontainerposition=parseInt(0);
        pagecontainerduration=parseInt(0);
        pagecontainerscrolltop=parseInt(0);
        pagecontainerscrollleft=parseInt(0);
        
        var obj=new ContainerItem();
        obj.PFrame=pagecontainerframe;
        obj.PPosition=pagecontainerposition;
        obj.PDuration=pagecontainerduration;
        obj.PScrollTop=pagecontainerscrolltop;
        obj.PScrollLeft=pagecontainerscrollleft;
        
//        if(obj!=null)
//        {
            ListAdd(pappcontrol.PCourseware.PTrack.PContainer.PItems,obj);
//        }
//    }
    
//   var cameraplayer=document.getElementById("MediaPlayer");
//    cameraplayer.url= pappcontrol.PCourseware.PMedia.PMovie.PUrl;
//    cameraplayer.controls.play();
    
    if(pappcontrol.PCourseware.PMedia.PAudio.PISIN == "false")
    {
       var img=document.getElementById("IsIN");
       img.src="Media/Camera.png";
       img.style.display="block";
       cameraplayer.height="45px";
    }
}

function eventnone()
{
    // window.event.cancelBubble=true;
    // window.event.returnValue=false;
    // return false;
}

function initevent()
{
    document.oncontextmenu=eventnone;
    document.onkeypress=eventnone;
    document.onkeydown=eventnone;
    document.onkeyup=eventnone;
    document.body.onkeypress=eventnone;
    document.body.onkeydown=eventnone;
    document.body.onkeyup=eventnone;
}

function closewindow()
{
    window.opener= null;
    window.open("", "_self");
    window.close();
}

function init(o)
{
    matchpage=document.getElementById("pageclassid").value==scriptclassid;
    if(!matchpage)
    {
        alert("页面和脚本不匹配！请清除缓存，如果还提示此信息请联系管理员更新！");
        var control=parent.pcontrol;
        if(control==null || control==undefined) closewindow();
        else parent.CloseWindow();
        return;
    }
    initevent();
    initcontent();
    initcontrol();
}

